<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\adminRepositoryInterface;
use App\Http\Traits\ApiResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\employee;
use App\email_content;

class adminRepository implements adminRepositoryInterface{

    // Use Trair To Desgin API's.
    use ApiResponseTrait;
    
    /** Group of model as vars */
    protected $employee_model;
    protected $email_content_model;
    
    /** Construct to handel inject models */
    public function __construct(employee $employee, email_content $email_content){
        $this->employee_model = $employee;
        $this->email_content_model = $email_content;
    }

    /** Employee Section [all - add - update - delete]*/

    // All employees Data...
    public function allEmployees(){

        /**
         * Build:[
         *  Seelct Data from employees table.
         *  return Data.
         * ]
         */

        $data = $this->employee_model::get(['id', 'name', 'email', 'title']);
        
        if($data){
            return $this->apiResponse(200, "Successfully", null, $data);
        }else{
            return $this->apiResponse(422, "unknwon error");
        }

    }


    // Add employee
    public function addEmployee($request){

         /**
         * Build:[
         *  request validation
         *  Save employee data into employees Table
         *  return employee Data.
         * ]
         */

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'name' => 'required|min:3',
            'title' => 'required',
            'email' => 'required|unique:employees|email',
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }
        
        $employee = $this->employee_model::create([
            "name" => $request->name,
            "title" => $request->title,
            "email" => $request->email,
        ]);

        if($employee){
            return $this->apiResponse(200, "Successfully Added", null, $employee);
        }else{
            return $this->apiResponse(422, "unknwon error");
        }
    }


    // update employee
    public function updateEmployee($request){

        /**
         * Build:[
         *  request validation
         *  update employee data into employees Table
         *  return employee Data.
         * ]
         */

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'name' => 'required|min:3',
            'title' => 'required',
            'employee_id' => 'required',
            'email' => 'required|unique:employees|email',
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }

        $employee = $this->employee_model::find($request->employee_id);

        if($employee){

            $employee->update([
                "name" => $request->name,
                "title" => $request->title,
                "email" => $request->email,
            ]);

            return $this->apiResponse(200, "Successfully Updated", null, $employee);
        }else{
            return $this->apiResponse(422, "notfound");
        }
    }


    // delete employee
    public function deleteEmployee($employee_id){
         /**
         * Build:[
         *  find employee
         *  delete employee
         * ]
         */

        $employee = $this->employee_model::find($employee_id);

        if($employee){

            $employee->delete();

            return $this->apiResponse(200, "Successfully Deleted");
        }else{
            return $this->apiResponse(422, "notfound");
        }
    }


    /** Email Content Section [all - add - update - delete]*/

    // All employees Data...
    public function allEmailContents(){

        /**
         * Build:[
         *  Seelct Data from email_contents table.
         *  return Data.
         * ]
         */

        $data = $this->email_content_model::get(['id', 'body', 'signature', 'type']);
        
        if($data){
            return $this->apiResponse(200, "Successfully", null, $data);
        }else{
            return $this->apiResponse(422, "unknwon error");
        }

    }


    // Add employee
    public function addEmailContent($request){

         /**
         * Build:[
         *  request validation
         *  Save employee data into email_content Table
         *  return employee Data.
         * ]
         */

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'body' => 'required|min:3',
            'signature' => 'required',
            'type' => 'required',
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }
        $email_content = $this->email_content_model::create([
            "body" => $request->body,
            "signature" => $request->signature,
            "type" => $request->type,
        ]);

        if($email_content){
            return $this->apiResponse(200, "Successfully Added", null, $email_content);
        }else{
            return $this->apiResponse(422, "unknwon error");
        }
    }


    // update employee
    public function updateEmailContent($request){

        /**
         * Build:[
         *  request validation
         *  update employee data into employees Table
         *  return employee Data.
         * ]
         */

        /** Validate The Requrments */
        $Validator = Validator::make($request->all(),[
            'body' => 'required|min:3',
            'signature' => 'required',
            'type' => 'required',
            'email_content_id' => 'required',
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422, "Validation Errors", $Validator->errors());
        }

        $email_content = $this->email_content_model::find($request->email_content_id);

        if($email_content){

            $email_content->update([
                "body" => $request->body,
                "signature" => $request->signature,
                "type" => $request->type,
            ]);

            return $this->apiResponse(200, "Successfully Updated", null, $email_content);
        }else{
            return $this->apiResponse(422, "notfound");
        }
    }


    // delete employee
    public function deleteEmailContent($email_content_id){
         /**
         * Build:[
         *  find employee
         *  delete employee
         * ]
         */

        $email_content = $this->email_content_model::find($email_content_id);

        if($email_content){

            $email_content->delete();

            return $this->apiResponse(200, "Successfully Deleted");
        }else{
            return $this->apiResponse(422, "notfound");
        }
    }


 
}